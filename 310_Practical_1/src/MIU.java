import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

/** A basic implementation of 
 *  the MIU system from Godel, Escher, Bach.
 * 
 * Author: Barry McAuley
 * Reg: 201106816
 *
 * Last edit: 03 March 2014
 */

public class MIU {
	
	// The list of next steps
	private Set<String> states;
	
	/**
	 * Constructor for the class
	 * 
	 * @since Feb 19, 2014
	 */
	public MIU() {
		states = new HashSet<String>();
	}
	
	/**
	 * Updates the states of the next available 
	 * routes in the MIU system
	 * 
	 * @param s
	 * @return Updated states
	 * @since Feb 19, 2014
	 */
	public Set<String> nextStates(String s) {
		states.clear();
		
		this.applyRuleOne(s);
		this.applyRuleTwo(s);
		this.applyRuleThree(s);
		this.applyRuleFour(s);
		
		return states;
	}
	
	/**
	 * Overloaded nextStates method it now takes
	 * in a path and returns an updated list of
	 * paths available to be taken
	 * 
	 * @param s
	 * @return
	 * @since Feb 19, 2014
	 */
	public List<List<String>> nextStates(List<String> s) {
		
		// The List of lists which we are returning...
		List<List<String>> result = new ArrayList<List<String>>();
		// The next states of the end of the path
		Set<String> nextSteps = this.nextStates( s.get(s.size()-1) );
		
		// Iterate through each String
		for (String res : nextSteps) {
			// The lists which we are adding to the result
			List<String> path = new ArrayList<String>();
			// Copy over the list
			path.addAll(s);
			
			// Add in the result
			path.add(res);
			
			// Add it to the result, the updated List
			result.add(path);
		}
		// Return the result...
		return result;
	}
	
	/**
	 * A heuristic function which estimates the number of
	 * steps remaining in our search
	 * 
	 * @param currentString
	 * @param goalString
	 * @return the number of steps
	 */
	public int estimateSteps(String currentString, String goalString) {
		
		// Variables
		int iCurr = 0;
		int iGoal = 0;
		
		// We should probbaly check if it is the goal...
		if (currentString.equals(goalString) ) {
			return 0;
		}
		
		// Next let's try applying each rule
		for (String str : this.nextStates(currentString)) {
			if (str.equals(goalString))
				return 1;
		}
		
		// Errr... Let's base it off the I's
		for (int i = 0; i < currentString.length(); i++) {
			if (currentString.charAt(i) == 'I') {
				iCurr++;
			}
		}
		
		for (int i = 0; i < goalString.length(); i++) {
			if (goalString.charAt(i) == 'I') {
				iGoal++;
			}
		}
		
		// Return
		if (iGoal == iCurr) {	
			return 2;
		} else if (iGoal > iCurr) {
			return 4;
		} else {
			return 3;
		}

	}
	 
	/**
	 * Breadth first search
	 * 
	 * @param s
	 * @return
	 * @since Feb 19, 2014
	 */
	public boolean breadthFirstSearch(String s) {
		
		// Variables
		List<String> current;
		int count = 0;
		Queue<List<String>> agenda = new LinkedList<List<String>>();
		Set<List<String>> explored = new HashSet<List<String>>();

		// Add our agenda
		List<String> path = new ArrayList<String>();
		path.add("MI");
		agenda.add(path);

		while (!agenda.isEmpty()) {
			
			count++; // Increment
			
			current = agenda.poll(); // Take the first item in the Queue
			explored.add(current); // Add to explored
			
			for (List<String> str : this.nextStates(current) ) {
				if (!agenda.contains(str) && !explored.contains(str)) {
					// Get the end of the path
					if (str.get(str.size()-1).equals(s)) {
						System.out.println("\nNumber of iterations: " + count);
						System.out.println("Size of agenda: " + agenda.size());
						System.out.println("Final path: " + str.toString());
						System.out.println("Length of final path: " + str.size());
						System.out.println("All explored: " + explored.toString());
						return true; // Return
					} else
						agenda.offer(str); // Add it to our agenda
				}
			}
				
		}
		
		return false;
		
	}
	
	/**
	 * Depth first search
	 * 
	 * @param s
	 * @return
	 * @since Feb 19, 2014
	 */
	public boolean depthFirstSearch(String s) {
		// Variables
		List<String> current;
		int count = 0;
		Stack<List<String>> agenda = new Stack<List<String>>();
		Set<List<String>> explored = new HashSet<List<String>>();

		// Add our agenda
		List<String> path = new ArrayList<String>();
		path.add("MI");
		agenda.add(path);

		while (true) {

			count++; // Increment

			if (agenda.isEmpty())
				return false;

			current = agenda.pop(); // Take the first item in the Stack
			explored.add(current); // Add to explored

			for (List<String> str : this.nextStates(current) ) {
				if (!agenda.contains(str) && !explored.contains(str)) {
					if (str.get(str.size()-1).equals(s)) {
						System.out.println("\nNumber of iterations: " + count);
						System.out.println("Size of agenda: " + agenda.size());
						System.out.println("Final path: " + str.toString());
						System.out.println("Length of final path: " + str.size());
						return true; // Return
					} else
						agenda.push(str); // Add it to our agenda
				
				}
			}

		}
	}
	
	/**
	 * Depth limited search
	 * 
	 * @param s
	 * @param n
	 * @return
	 * @since Feb 19, 2014
	 */
	public boolean depthLimitedSearch(String s, int n) {
		// Prepare the initial state
		List<String> initial = new ArrayList<String>();
		initial.add("MI");
		
		return recursiveDepthLimited(initial , s, n);
	}
	
	/** Recursive implementation of DLS to be used */
	private boolean recursiveDepthLimited (List<String> curr, String goal, int n) {
		// Initial test
		if (curr.get(curr.size()-1).equals(goal)) {
			System.out.println("\nFinal path: " + curr.toString());
			System.out.println("Length of final path: " + curr.size());
			return true;
		} else if (n == 0 )
			return false;
		else {
			
			for (List<String> s : this.nextStates(curr) ) {
				boolean result = recursiveDepthLimited(s, goal, n-1);
				// Check if we've got our goal
				if (result) 
					return result;
				
			}
			
			return false;
			
		}
	}

	/**
	 * Iterative deepening search
	 * 
	 * @param s
	 * @return
	 * @since Feb 19, 2014
	 */
	public boolean iteraiveDeepeningSearch(String s) {
		
		// Variables
		int n = 0;
		
		while (true) {
			// Check for result
			if (this.depthLimitedSearch(s, n)) {
				System.out.println("Value of n: " + n);
				return true;
			} 
				
			n++; // Increment
		}
		
	}
	
	/**
	 * Best first search implementation.
	 * 
	 * @param s
	 * @return
	 */
	public boolean bestFirstSearch(String s) {
		// Variables
		List<String> current;
		Queue<List<String>> agenda = new LinkedList<List<String>>();
		Set<List<String>> explored = new HashSet<List<String>>();
		int count = 0;

		// Add in the initial state
		List<String> path = new ArrayList<String>();
		path.add("MI");
		agenda.add(path);
		explored.add(path);

		// Still stuff is in the agenda...
		while (!agenda.isEmpty()) {
			// Increment
			count++; 
			// Set the current as the first in the index
			current = agenda.peek();

			// Select the best
			for (List<String> p : agenda) {
				if ( estimateSteps( p.get(p.size()-1), s) < estimateSteps(current.get(current.size()-1), s) ) {
					current = p;
				}
				
			}

			// Remove the current
			agenda.remove(current);
			explored.add(current);

			// Check to see if we have found the goal
			if (current.get(current.size()-1).equals(s)) {
				System.out.println("Size of agenda: " + agenda.size());
				System.out.println("Iterations: " + count);
				System.out.println("Final path: " + current.toString());
				System.out.println("Length of final path: " + current.size());
				System.out.println("All explored: " + explored.toString());
				return true;
			} else {
				// It's not the goal, move on
				for (List<String> str : nextStates(current)) {
					if (!agenda.contains(str) && !explored.contains(str))
						agenda.offer(str);
				}
			}

		}

		return false;
	}
	
	/**
	 * An implementation of A* search
	 * 
	 * @param s
	 * @return
	 */
	public boolean aStarSearch(String s) {
		// Variables
		List<String> current;
		Queue<List<String>> agenda = new LinkedList<List<String>>();
		Set<List<String>> explored = new HashSet<List<String>>();
		int count = 0;
		
		// Add in the initial state
		List<String> path = new ArrayList<String>();
		path.add("MI");
		agenda.add(path);
		explored.add(path);

		// Still stuff is in the agenda...
		while (!agenda.isEmpty()) {
			// Increment
			count++; 
			// Set the current as the first in the index
			current = agenda.peek();
			
			// Select the best
			for (List<String> p : agenda) {
				if ( p.size() + estimateSteps( p.get(p.size()-1), s) <  current.size() + estimateSteps(current.get(current.size()-1), s) ) {
					current = p;
				}
			}
			
			// Remove the current
			agenda.remove(current);
			explored.add(current);

			// Check to see if we have found the goal
			if (current.get(current.size()-1).equals(s)) {
				System.out.println("Size of agenda: " + agenda.size());
				System.out.println("Iterations: " + count);
				System.out.println("Final path: " + current.toString());
				System.out.println("Length of final path: " + current.size());
				System.out.println("All explored: " + explored.toString());
				return true;
			} else {
				// It's not the goal, move on
				for (List<String> str : nextStates(current)) {
					if (!agenda.contains(str) && !explored.contains(str))
						agenda.offer(str);
				}
			}
			
		}
			
		return false;
	}
	
	
	/**
	 * Rules of the system
	 * 
	 * @param s
	 */
	private void applyRuleOne (String s) {
		// Test to see if the string ends in an I
		if (s.endsWith("I"))
			states.add(s.concat("U"));
	}
	
	private void applyRuleTwo (String s) {
		// Test to see if the string starts in an M
		if (s.startsWith("M")) 
			states.add(s.concat(s.substring(1)));
	}
	
	private void applyRuleThree (String s) {
		
		String newS;
			for (int i = 0; i < s.length()-2; i++) {
				if (s.charAt(i) == 'I' && s.charAt(i+1) == 'I' && s.charAt(i+2) == 'I') {
					newS = s.substring(0, i) + "U" + s.substring(i+3);
					states.add(newS);
				}
			}
	}

	
	private void applyRuleFour (String s) {
		
		String newS;
		for (int i = 0; i < s.length()-1; i++) {
				if (s.charAt(i) == 'U' && s.charAt(i+1) == 'U') {
					newS = s.substring(0, i) + "" + s.substring(i+2);
					states.add(newS);
				}
		}
	}

}
