import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main(String[] args) {

		MIU m = new MIU(); 
		List<String> input = new ArrayList<String>();
		input.add("MI"); input.add("MII");
		
		System.out.println("next-states(\"MI\") -> " + m.nextStates("MI").toString());
		System.out.println("next-states(\"MIU\") -> " + m.nextStates("MIU").toString());
		System.out.println("next-states(\"MUI\") -> " + m.nextStates("MUI").toString());
		System.out.println("next-states(\"MIIII\") -> " + m.nextStates("MIIII").toString());
		System.out.println("next-states(\"MUUII\") -> " + m.nextStates("MUUII").toString());
		System.out.println("next-states(\"MUUIUU\") -> " + m.nextStates("MUUIUU").toString());
		System.out.println("next-states(\"[MI, MII]\") -> " + m.nextStates(input).toString());
		
		System.out.println("\nBreadth first: ");
		m.breadthFirstSearch("MIIUIIIU");
		// m.depthFirstSearch("MII");
		// m.iteraiveDeepeningSearch("MIIU");
		
		System.out.println("\nBest first search: ");
		m.bestFirstSearch("MIIUIIIU");
		
		System.out.println("\nA* search: ");
		m.aStarSearch("MIIUIIIU");
		
	}

}
