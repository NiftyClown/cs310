(define (problem traderprobB2) (:domain Trader)
(:objects
	t - Trader
	t2 - Trader
	item1 - item
	item2 - item
	item3 - item
	item4 - item
	item5 - item
	item6 - item
	item7 - item
	locationX - location
	locationY - location
	marketL - market
	marketM - market
	marketN - market
	marketO - market
	marketP - market
	marketQ - market
)
(:init
	(at t locationX)
	(at t2 locationY)
	(at item1 marketL)
	(at item2 marketL)
	(at item3 marketQ)
	(at item4 marketL)
	(at item5 marketP)
	(at item6 marketN)
	(at item7 marketM)
)

(:goal 
	(and
		(at item2 marketM)
		(at item4 marketN)
		(at item3 marketO)
		(at item1 marketO)
		(at item5 marketP)
		(at item6 marketO)
		(at item7 marketQ)
		(sold item1 t marketO)
		(sold item3 t marketO)
		(sold item2 t marketM)
		(sold item4 t marketN)
		(sold item4 t2 marketN)
		(sold item5 t2 marketP)
		(sold item6 t2 marketO)
		(sold item7 t marketQ)
		(at t locationY)
		(at t2 locationX)
	)
)

)
