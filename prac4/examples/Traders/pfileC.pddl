(define (problem traderprobC) (:domain Trader)
(:objects
	t - Trader
	item1 - item
	item2 - item
	item3 - item
	item4 - item
	locationX - location
	locationO - fuelstation
	marketL - market
	marketM - market
	marketN - market
	marketO - market

	time0 - time
	time1 - time
	time2 - time
	time3 - time
	time4 - time
	time5 - time
	time6 - time
	time7 - time
	time8 - time
	time9 - time
	time10 - time
	time11 - time
	time12 - time
	time13 - time
	time14 - time
	time15 - time
	time16 - time
	time17 - time
	time18 - time
	time19 - time
	time20 - time

	e0 - energy
	e1 - energy
	e2 - energy
	e3 - energy
)

(:init
	(at t locationX)
	(at item1 marketL)
	(at item2 marketL)
	(at item3 marketM)
	(at item4 marketN)

	(time-now time0)
	(energy-now e2)

	(next-time time0 time1)
	(next-time time1 time2)
	(next-time time2 time3)
	(next-time time3 time4)
	(next-time time4 time5)
	(next-time time5 time6)
	(next-time time6 time7)
	(next-time time7 time8)
	(next-time time8 time9)
	(next-time time9 time10)
	(next-time time10 time11)
	(next-time time11 time12)
	(next-time time12 time13)
	(next-time time13 time14)
	(next-time time14 time15)
	(next-time time15 time16)
	(next-time time16 time17)
	(next-time time17 time18)
	(next-time time18 time19)
	(next-time time19 time20)

	(next-energy e4 e3)
	(next-energy e3 e2)
	(next-energy e2 e1)
	(next-energy e1 e0)
)

(:goal 
	(and
		(at item2 marketM)
		(at item4 marketN)
		(at item3 marketO)
		(at item1 marketO)
		(sold item1 t marketL)
		(sold item3 t marketL)
		(sold item2 t marketM)
		(sold item4 t marketN)
		(at t locationX)
	)
)

)
