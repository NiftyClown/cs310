(define (domain Trader)
(:requirements :strips :typing :fluents :durative-actions :equality)

(:types 
	locatable place - object
	item trader - locatable
	market location fuelstation - place
)

(:predicates 
	     (at ?x - locatable ?y - place)
	     (has ?i - item ?t - trader)
	     (sold ?i - item ?t - trader ?m - market)
)

(:functions
	(energy ?t - trader)
	(capacity ?t - trader)
	(currentHolding ?t - trader)
)


(:durative-action BUY
:parameters (?t - trader ?i - item ?m - market)
:duration (= ?duration 1)
:condition (and (over all (at ?t ?m)) (at start (at ?i ?m)) (at start (< (currentHolding ?t ) (capacity ?t)) ) )
:effect (and (at end (has ?i ?t)) (at start (not (at ?i ?m))) (at start (increase (currentHolding ?t) 1)) )
)

(:durative-action SELL
:parameters (?t - trader ?i - item ?m - market)
:duration (= ?duration 1)
:condition (and  (over all (at ?t ?m))  (at start (has ?i ?t)) (at start (> (currentHolding ?t) 0)) )
:effect (and (at end (not (has ?i ?t))) (at end (sold ?i ?t ?m)) (at end (at ?i ?m)) (at start (decrease (currentHolding ?t) 1)) )
)

(:durative-action REFUEL
:parameters (?t - trader ?f - fuelstation)
:duration (= ?duration 1)
:condition (and (over all (at ?t ?f)) )
:effect (and (at end (assign (energy ?t) 4)) )
)
	     
(:durative-action WALK
:parameters (?t - trader ?y ?z - place) 
:duration (= ?duration 2)
:condition (and (at start (at ?t ?y)) (at start (>= (energy ?t) 1) ))
:effect (and (at start (not (at ?t ?y))) (at end (at ?t ?z)) (at start (decrease (energy ?t) 1) ) )
)

)