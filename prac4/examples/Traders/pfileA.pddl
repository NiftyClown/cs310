(define (problem traderprobA) (:domain Trader)
(:objects
	t - Trader
	item1 - item
	item2 - item
	item3 - item
	item4 - item
	locationX - location
	marketL - market
	marketM - market
	marketN - market
	marketO - market
)

(:init
	(at t locationX)
	(at item1 marketL)
	(at item2 marketL)
	(at item3 marketM)
	(at item4 marketN)
)

(:goal 
	(and
		(at item2 marketM)
		(at item4 marketN)
		(at item3 marketO)
		(at item1 marketO)
		(sold item1 t marketO)
		(sold item3 t marketO)
		(sold item2 t marketM)
		(sold item4 t marketN)
		(at t locationX)
	)
)

)
