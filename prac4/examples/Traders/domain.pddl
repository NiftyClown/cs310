(define (domain Trader)
(:requirements :strips :typing)

(:types 
	locatable place - object
	item trader - locatable
	market location - place
)

(:predicates 
	     (at ?x - locatable ?y - place)
	     (has ?i - item ?t - trader)
	     (sold ?i - item ?t - trader ?m - market)
)

(:action BUY
:parameters (?t - trader ?i - item ?m - market)
:precondition (and (at ?t ?m) (at ?i ?m) )
:effect (and (has ?i ?t) (not (at ?i ?m)) )
)

(:action SELL
:parameters (?t - trader ?i - item ?m - market)
:precondition (and (at ?t ?m) (has ?i ?t) )
:effect (and (not (has ?i ?t)) (sold ?i ?t ?m) (at ?i ?m) )
)
	     
(:action WALK
:parameters (?t - trader ?y ?z - place) 
:precondition (and (at ?t ?y) )
:effect (and (not (at ?t ?y)) (at ?t ?z) )
)

)