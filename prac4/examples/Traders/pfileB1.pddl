(define (problem traderprobB1) (:domain Trader)
(:objects
	t - Trader
	item1 - item
	item2 - item
	item3 - item
	item4 - item
	item5 - item
	item6 - item
	locationX - location
	locationY - location
	marketL - market
	marketM - market
	marketN - market
	marketO - market
	marketP - market
)
(:init
	(at t locationX)
	(at item1 marketL)
	(at item2 marketL)
	(at item3 marketM)
	(at item4 marketN)
	(at item5 marketP)
	(at item6 marketN)
)

(:goal 
	(and
		(at item2 marketM)
		(at item4 marketN)
		(at item3 marketO)
		(at item1 marketO)
		(at item5 marketP)
		(at item6 marketO)
		(sold item1 t marketO)
		(sold item3 t marketO)
		(sold item2 t marketM)
		(sold item4 t marketN)
		(sold item5 t marketP)
		(sold item6 t marketO)
		(at t locationY)
	)
)

)
