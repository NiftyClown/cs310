(define (domain Trader)
(:requirements :strips :typing)

(:types 
	locatable place time energy - object
	item trader - locatable
	market location fuelstation - place
)

(:constants
	e4 - energy
)

(:predicates 
	     (at ?x - locatable ?y - place)
	     (has ?i - item ?t - trader)
	     (sold ?i - item ?t - trader ?m - market)
	     (next-time ?t1 - time ?t2 - time)
	     (next-energy ?e1 - energy ?e2 - energy)
	     (recharge-value ?e1 - energy ?m - market)
)

(:functions
	(time-now ?t - time)
	(energy-now ?e - energy)
)

(:action BUY
:parameters (?t - trader ?i - item ?m - market)
:precondition (and (at ?t ?m) (at ?i ?m))
:effect (and (has ?i ?t) (not (at ?i ?m)) )
)

(:action SELL
:parameters (?t - trader ?i - item ?m - market)
:precondition (and (at ?t ?m) (has ?i ?t) )
:effect (and (not (has ?i ?t)) (sold ?i ?t ?m) (at ?i ?m) )
)
	     
(:action WALK
:parameters (?t - trader ?y ?z - place ?t1 ?t2 - time ?e1 ?e2 - energy) 
:precondition (and (at ?t ?y) (time-now ?t1) (energy-now ?e1) (next-energy ?e1 ?e2) (next-time ?t1 ?t2) )
:effect (and (not (at ?t ?y)) (at ?t ?z) (not(time-now ?t1)) (time-now ?t2) (not(energy-now ?e1)) (energy-now ?e2) )
)

(:action REFUEL
:parameters (?t - trader ?f - fuelstation ?e - energy)
:precondition (and (at ?t ?f) (energy-now ?e))
:effect (and (energy-now e4) (not(energy-now ?e)) )
)

)