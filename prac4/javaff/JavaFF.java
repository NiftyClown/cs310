/************************************************************************
 * Strathclyde Planning Group,
 * Department of Computer and Information Sciences,
 * University of Strathclyde, Glasgow, UK
 * http://planning.cis.strath.ac.uk/
 * 
 * Copyright 2007, Keith Halsey
 * Copyright 2008, Andrew Coles and Amanda Smith
 *
 * (Questions/bug reports now to be sent to Andrew Coles)
 *
 * This file is part of JavaFF.
 * 
 * JavaFF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * JavaFF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JavaFF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ************************************************************************/

package javaff;

import javaff.data.PDDLPrinter;
import javaff.data.UngroundProblem;
import javaff.data.GroundProblem;
import javaff.data.Plan;
import javaff.data.TotalOrderPlan;
import javaff.data.TimeStampedPlan;
import javaff.parser.PDDL21parser;
import javaff.planning.State;
import javaff.planning.TemporalMetricState;
import javaff.planning.RelaxedTemporalMetricPlanningGraph;
import javaff.planning.HelpfulFilter;
import javaff.planning.NullFilter;
import javaff.scheduling.Scheduler;
import javaff.scheduling.JavaFFScheduler;
import javaff.search.Search;
import javaff.search.BestFirstSearch;
import javaff.search.EnforcedHillClimbingSearch;
import javaff.search.HillClimbingSearch;
import javaff.search.DepthBoundedHillClimbingSearch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Random;

public class JavaFF
{
    public static BigDecimal EPSILON = new BigDecimal(0.01);
	public static BigDecimal MAX_DURATION = new BigDecimal("100000"); //maximum duration in a duration constraint
	public static boolean VALIDATE = false;

	public static Random generator = null;
 
	public static PrintStream planOutput = System.out;
	public static PrintStream parsingOutput = System.out;
	public static PrintStream infoOutput = System.out;
	public static PrintStream errorOutput = System.err;

	public static void main (String args[]) {
		EPSILON = EPSILON.setScale(2,BigDecimal.ROUND_HALF_EVEN);
		MAX_DURATION = MAX_DURATION.setScale(2,BigDecimal.ROUND_HALF_EVEN);
		
		generator = new Random();
		
		if (args.length < 2) {
			System.out.println("Parameters needed: domainFile.pddl problemFile.pddl [random seed] [outputfile.sol");

		} else {
			File domainFile = new File(args[0]);
			File problemFile = new File(args[1]);
			File solutionFile = null;
			if (args.length > 2)
			{
				generator = new Random(Integer.parseInt(args[2]));
			}
	
			if (args.length > 3)
			{
				solutionFile = new File(args[3]);
			}
	
			Plan plan = plan(domainFile,problemFile);
	
			if (solutionFile != null && plan != null) writePlanToFile(plan, solutionFile);
			
		}
	}


    public static Plan plan(File dFile, File pFile)
    {
	// get user to select their preferred search option
	infoOutput.println("Please type in a number corresponding to your preferred search option.\n");
	infoOutput.println("Type 1 for the default FF search (Enforced Hill Climbing with helpful actions followed by Best First search).");
	infoOutput.println("Type 2 for Enforced Hill Climbing with all actions followed by Best First search.");
	infoOutput.println("Type 3 for Best First search.");
	infoOutput.println("Type 4 for Hill Climbing search.");
	infoOutput.println("Type 5 for Depth Bounded Hill Climbing search.");
	infoOutput.println("Type 6 for Depth Bounded Hill Climbing with Restarts.");

	int searchvalue = 0;

	BufferedReader searchInput = new BufferedReader(new InputStreamReader (System.in));
	try {
	         searchvalue = Integer.parseInt(searchInput.readLine());
	} catch (NumberFormatException nfe) {
	    infoOutput.println ("The search option you entered is not recognised. ByeBye.\n");
	    return null;
	} catch (IOException ioe) {
	    errorOutput.println(ioe);
	    ioe.printStackTrace();
	}

		// ********************************
		// Parse and Ground the Problem
		// ********************************

		long startTime = System.currentTimeMillis();
		
		UngroundProblem unground = PDDL21parser.parseFiles(dFile, pFile);

		if (unground == null)
		{
			System.out.println("Parsing error - see console for details");
			return null;
		}


		//PDDLPrinter.printDomainFile(unground, System.out);
		//PDDLPrinter.printProblemFile(unground, System.out);

		GroundProblem ground = unground.ground();

		long afterGrounding = System.currentTimeMillis();

		// ********************************
		// Search for a plan
		// ********************************

		// Get the initial state
		TemporalMetricState initialState = ground.getTemporalMetricInitialState();
		
                State goalState = goalState = performFFSearch(initialState,searchvalue);
                
		long afterPlanning = System.currentTimeMillis();

                TotalOrderPlan top = null;
		if (goalState != null) top = (TotalOrderPlan) goalState.getSolution();
		if (top != null) top.print(planOutput);


		/*javaff.planning.PlanningGraph pg = initialState.getRPG();
		Plan plan  = pg.getPlan(initialState);
		plan.print(planOutput);
		return null;*/

		// ********************************
		// Schedule a plan
		// ********************************

                //TimeStampedPlan tsp = null;
                
                //if (goalState != null)
                //{
                   
                   //infoOutput.println("Scheduling");
		
                   //Scheduler scheduler = new JavaFFScheduler(ground);
                   //tsp = scheduler.schedule(top);
                //}
                

		//long afterScheduling = System.currentTimeMillis();
		
		//if (tsp != null) tsp.print(planOutput);

		double groundingTime = (afterGrounding - startTime)/1000.00;
		double planningTime = (afterPlanning - afterGrounding)/1000.00;
		//double schedulingTime = (afterScheduling - afterPlanning)/1000.00;
		//		if (goalState == null) {
		//		    infoOutput.println("The search strategy was unable to find a plan.");}
		infoOutput.println("Instantiation Time =\t\t"+groundingTime+"sec");
		infoOutput.println("Planning Time =\t"+planningTime+"sec");
		//infoOutput.println("Scheduling Time =\t"+schedulingTime+"sec");

		
		return top;
	}

	private static void writePlanToFile(Plan plan, File fileOut)
    {
		try
	    {
			FileOutputStream outputStream = new FileOutputStream(fileOut);
			fileOut.delete();
			fileOut.createNewFile();
			PrintWriter printWriter = new PrintWriter(outputStream);
			plan.print(printWriter);
			printWriter.close();
		}
		catch (FileNotFoundException e)
	    {
			errorOutput.println(e);
			e.printStackTrace();
		}
		catch (IOException e)
	    {
			errorOutput.println(e);
			e.printStackTrace();
	       	}

    }
    
    public static State performFFSearch(TemporalMetricState initialState, int searchoption) {

	switch (searchoption) {
	case 1:
	    // Implementation of standard FF-style search
	    infoOutput.println("Performing search as in FF - first considering Enforced Hill Climbing with only helpful actions");
	
	    // Now, initialise an EHC searcher
	    EnforcedHillClimbingSearch EHCS = new EnforcedHillClimbingSearch(initialState);

	    EHCS.setFilter(HelpfulFilter.getInstance()); // and use the helpful actions neighbourhood
 
	    // Try and find a plan using EHC
	    State goalState = EHCS.search();

	    if (goalState == null) // if we can't find one
		{
		    infoOutput.println("Enforced Hill Climbing failed, using Best First search, with all actions\n");
		
		    // create a Best-First Searcher
		    BestFirstSearch BFS = new BestFirstSearch(initialState);
		
		    // ... change to using the 'all actions' neighbourhood (a null filter, as it removes nothing)
		
		    BFS.setFilter(NullFilter.getInstance());
		
		    // and use that
		    goalState = BFS.search();
		}
	
	    return goalState; // return the plan 
	    // break;
	case 2:
	    // Implementation of FF-style search but without helpful actions
	    infoOutput.println("Performing search as in FF but with Enforced Hill Climbing considering all actions\n");
	
	    // Now, initialise an EHC searcher
	    EHCS = new EnforcedHillClimbingSearch(initialState);

	    // For Part 1, Exercise 2
	    EHCS.setFilter(HelpfulFilter.getInstance()); // and use the 'all actions' neighbourhood
 
	    // Try and find a plan using EHC
	    goalState = EHCS.search();

	    if (goalState == null) // if we can't find one
		{
		    infoOutput.println("Enforced Hill Climbing failed, using Best First search, with all actions\n");
		
		    // create a Best-First Searcher
		    BestFirstSearch BFS = new BestFirstSearch(initialState);
		
		    // ... change to using the 'all actions' neighbourhood (a null filter, as it removes nothing)
		
		    BFS.setFilter(NullFilter.getInstance());
		
		    // and use that
		    goalState = BFS.search();
		}
	
	    return goalState; // return the plan 
	    // break;
	case 3:
	    infoOutput.println("Using Best First search, with all actions\n");
		
	    // create a Best-First Searcher
	    BestFirstSearch BFS1 = new BestFirstSearch(initialState);
		
	    // ... change to using the 'all actions' neighbourhood (a null filter, as it removes nothing)
		
	    BFS1.setFilter(NullFilter.getInstance());
		
	    // and use that
	    goalState = BFS1.search();
	
	    return goalState; // return the plan 
	    // break;
	case 4: 
	    infoOutput.println("Performing Hill Climbing search\n");

	    // Now, initialise a Hill Climbing searcher
	    HillClimbingSearch HCS = new HillClimbingSearch(initialState);

	    HCS.setFilter(NullFilter.getInstance()); // and use the 'all actions neighbourhood

	    // Try and find a plan using Hill Climbing search
	    goalState = HCS.search();
	    if (goalState == null) {
		    infoOutput.println("Hill Climbing search was unable to find a plan.");}
	    return goalState; // return the plan
	case 5: 
	    infoOutput.println("Performing Depth Bounded Hill Climbing search\n");

	    int howDeep = 30; // set the maximum depth
	    // Now, initialise a Hill Climbing searcher
	    DepthBoundedHillClimbingSearch DBHCS = new DepthBoundedHillClimbingSearch(initialState, howDeep);

	    DBHCS.setFilter(NullFilter.getInstance()); // and use the 'all actions neighbourhood
	   
	    //	    DBHCS.setDepth(howDeep);

	    // Try and find a plan using Hill Climbing search
	    goalState = DBHCS.search();
	    if (goalState == null) {
		    infoOutput.println("Depth Bounded Hill Climbing search was unable to find a plan.");}
	    return goalState; // return the plan
	case 6:
	    infoOutput.println("Performing Depth Bounded Hill Climbing search with Restarts\n");

	    State goalState1 = null;

	    for (int depthBound = 5; depthBound < 100; ++depthBound) {

		DepthBoundedHillClimbingSearch DBHCS2 = new DepthBoundedHillClimbingSearch(initialState);

		DBHCS2.setFilter(NullFilter.getInstance()); // and use the 'all actions neighbourhood

		DBHCS2.setDepth(depthBound); // set the max depth
	   
		// Try and find a plan using Depth Bounded Hill Climbing search with Restarts
		goalState1 = DBHCS2.search();
		if (goalState1 != null) {
		    return goalState1;
		}		
	    }
	    infoOutput.println("Depth Bounded Hill Climbing search with Restarts has failed to find a plan.\n");
	    return goalState1;
	    default:
		System.out.println("Search option not recognised - defaulting to FF-style search.\n ");
		infoOutput.println("Performing search as in FF - first considering EHC with only helpful actions");
	
		// Now, initialise an EHC searcher
		EHCS = new EnforcedHillClimbingSearch(initialState);

		EHCS.setFilter(HelpfulFilter.getInstance()); // and use the helpful actions neighbourhood

		// Try and find a plan using EHC
		goalState = EHCS.search();

		if (goalState == null) // if we can't find one
		    {
			infoOutput.println("EHC failed, using best-first search, with all actions\n");
		
			// create a Best-First Searcher
			BestFirstSearch BFS = new BestFirstSearch(initialState);
		
			// ... change to using the 'all actions' neighbourhood (a null filter, as it removes nothing)
		
			BFS.setFilter(NullFilter.getInstance());
		
			// and use that
			goalState = BFS.search();
			//return goalState; // return the plan 
		    }
		return goalState; // return the plan
	    
	}

    }
		
}
